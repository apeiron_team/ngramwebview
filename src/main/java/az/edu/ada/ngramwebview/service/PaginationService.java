package az.edu.ada.ngramwebview.service;


import az.edu.ada.ngramwebview.model.*;
import az.edu.ada.ngramwebview.repo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class PaginationService {

    @Autowired
    UnigramRepo unigramRepository;
    @Autowired
    BigramRepo bigramRepo;
    @Autowired
    ThreegramRepo threegramRepo;
    @Autowired
    FourgramRepo fourgramRepo;
    @Autowired
    FivegramRepo fivegramRepo;


    public Page<? extends Gram> findAll(int gram, int page, int size) {
        switch (gram) {
            case 1:
                return findUnigramPagable(page, size);
            case 2:
                return findBigramPagable(page, size);
            case 3:
                return findThreegramPagable(page, size);
            case 4:
                System.out.println(gram);
                return findFourgramPagable(page, size);
            case 5:
                return findFivegramPagable(page, size);
            default:
                return null;
        }
    }


    public Page<Unigram> findUnigramPagable(int page, int size) {
        return unigramRepository.findAll(new PageRequest(page, size, Sort.by(Sort.Order.desc("count"))));
    }

    public Page<Bigram> findBigramPagable(int page, int size) {
        return bigramRepo.findAll(new PageRequest(page, size, Sort.by(Sort.Order.desc("count"))));
    }

    Page<Threegram> findThreegramPagable(int page, int size) {
        return threegramRepo.findAll(new PageRequest(page, size, Sort.by(Sort.Order.desc("count"))));
    }

    Page<Fourgram> findFourgramPagable(int page, int size) {
        return fourgramRepo.findAll(new PageRequest(page, size, Sort.by(Sort.Order.desc("count"))));
    }

    Page<Fivegram> findFivegramPagable(int page, int size) {
        return fivegramRepo.findAll(new PageRequest(page, size, Sort.by(Sort.Order.desc("count"))));
    }


}
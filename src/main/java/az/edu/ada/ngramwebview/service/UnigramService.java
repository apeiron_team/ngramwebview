package az.edu.ada.ngramwebview.service;

import az.edu.ada.ngramwebview.model.Unigram;
import az.edu.ada.ngramwebview.repo.UnigramRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UnigramService {

    @Autowired
    UnigramRepo unigramRepo;

    public List<Unigram> findAllUnigram(){
       return (List<Unigram>) unigramRepo.findAll();

    }


}

package az.edu.ada.ngramwebview.service;

import az.edu.ada.ngramwebview.model.Bigram;
import az.edu.ada.ngramwebview.repo.BigramRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BigramService {
    @Autowired
    BigramRepo bigramRepo;

    public List<Bigram> getall() {
        return bigramRepo.findAll();
    }
}

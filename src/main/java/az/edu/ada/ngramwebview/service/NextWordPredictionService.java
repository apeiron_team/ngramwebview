package az.edu.ada.ngramwebview.service;

import az.edu.ada.ngramwebview.model.Gram;
import az.edu.ada.ngramwebview.model.Unigram;
import az.edu.ada.ngramwebview.repo.BigramRepo;
import az.edu.ada.ngramwebview.repo.UnigramRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NextWordPredictionService {

    @Autowired
    UnigramRepo unigramRepo;

    @Autowired
    BigramRepo bigramRepo;


    public List<? extends Gram> findNextWord(String text) {
        System.out.println("data:"+text);

        if (text.contains(" ")) {
            System.out.println("+ exist");
            String[] arry=text.split("\\s");
            return bigramRepo.findSecond(arry[0]);
        } else {
            System.out.println("+ isnot");
            return unigramRepo.findFirst(text);
        }


//        String[] array = text.split(" ");
//        switch (array.length) {
//            case 1:
//                return unigramRepo.findFirst(text);
//            case 2:
//                return bigramRepo.findSecond(array[0]);
//            default:
//                return null;
//
//        }
    }
}

package az.edu.ada.ngramwebview.service;

import az.edu.ada.ngramwebview.model.Bigram;
import az.edu.ada.ngramwebview.model.Threegram;
import az.edu.ada.ngramwebview.repo.BigramRepo;
import az.edu.ada.ngramwebview.repo.ThreegramRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ThreegramService {

    @Autowired
    ThreegramRepo threegramRepo;
    public List<Threegram> getall() {
        return threegramRepo.findAll();
    }
}

package az.edu.ada.ngramwebview.controller;

import az.edu.ada.ngramwebview.service.BigramService;
import az.edu.ada.ngramwebview.service.ThreegramService;
import az.edu.ada.ngramwebview.service.UnigramService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class PageController {

    @Autowired
    UnigramService unigramService;
    @Autowired
    BigramService bigramService;
    @Autowired
    ThreegramService threegramService;

    @GetMapping("/")
    public String index() {
        return "ngram";
    }

    @GetMapping("res/{gram}")
    public String res(@PathVariable("gram") int ngram, Model model) {
        model.addAttribute("gram",ngram);
        return "Result";
    }

    @GetMapping("result/{gram}")
    public String result(@PathVariable("gram") int ngram, Model model) {

        if (ngram == 1) {
            model.addAttribute("ngram", unigramService.findAllUnigram());
            return "table";

        }
        if (ngram == 2) {
            model.addAttribute("ngram", bigramService.getall());
            return "table2";
        }

        if (ngram == 3) {
            model.addAttribute("ngram", threegramService.getall());
            return "table3";
        }
        return null;
    }
}

package az.edu.ada.ngramwebview.controller;

import az.edu.ada.ngramwebview.model.Gram;
import az.edu.ada.ngramwebview.service.NextWordPredictionService;
import az.edu.ada.ngramwebview.service.PaginationService;
import az.edu.ada.ngramwebview.service.UnigramService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class RestAPIController {
    @Autowired
    UnigramService unigramService;
    @Autowired
    NextWordPredictionService predictionService;
    //    BigramService bigramService;
    @Autowired
    PaginationService paginationService;


    @GetMapping("/api/gram")
    public Page<? extends Gram> unigram(@RequestParam(required = false, defaultValue = "1") int n,
                                        @RequestParam(required = false, defaultValue = "0") int page,
                                        @RequestParam(required = false, defaultValue = "20") int size) {
        return paginationService.findAll(n, page, size);

    }

    @GetMapping("/nextWord")
    public List<? extends Gram> nextWord(@RequestParam(required = false) String text) {

        return predictionService.findNextWord(text);

    }





}

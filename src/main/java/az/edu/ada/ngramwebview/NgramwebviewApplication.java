package az.edu.ada.ngramwebview;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NgramwebviewApplication {

    public static void main(String[] args) {
        SpringApplication.run(NgramwebviewApplication.class, args);
    }

}

package az.edu.ada.ngramwebview.repo;

import az.edu.ada.ngramwebview.model.Threegram;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ThreegramRepo extends PagingAndSortingRepository<Threegram, Integer> {
    Page<Threegram> findAll(Pageable pageable);

    List<Threegram> findAll();

}

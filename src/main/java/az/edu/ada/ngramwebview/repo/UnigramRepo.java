package az.edu.ada.ngramwebview.repo;

import az.edu.ada.ngramwebview.model.Unigram;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UnigramRepo extends PagingAndSortingRepository<Unigram, Integer> {
    Page<Unigram> findAll(Pageable pageable);

    List<Unigram> findAll();

    @Query(value = "select * from unigram  where firstword like :input% order by count DESC LIMIT 5",
            nativeQuery = true)
    List<Unigram> findFirst(@Param("input") String word);
}

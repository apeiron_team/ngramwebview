package az.edu.ada.ngramwebview.repo;

import az.edu.ada.ngramwebview.model.Fivegram;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FivegramRepo extends PagingAndSortingRepository<Fivegram, Integer> {
    Page<Fivegram> findAll(Pageable pageable);

    List<Fivegram> findAll();
}


package az.edu.ada.ngramwebview.repo;

import az.edu.ada.ngramwebview.model.Fourgram;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FourgramRepo extends PagingAndSortingRepository<Fourgram, Integer> {
    Page<Fourgram> findAll(Pageable pageable);

    List<Fourgram> findAll();
}

package az.edu.ada.ngramwebview.repo;

import az.edu.ada.ngramwebview.model.Bigram;
import az.edu.ada.ngramwebview.model.Unigram;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;

public interface BigramRepo extends PagingAndSortingRepository<Bigram,Integer> {
    Page<Bigram> findAll(Pageable pageable);
    List<Bigram> findAll();

    @Query(value = "select * from bigram  where firstword = :input order by count DESC LIMIT 10",
            nativeQuery = true)
    List<Bigram> findSecond(@Param("input") String word);

}

package az.edu.ada.ngramwebview.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import java.io.Serializable;

@Entity
@IdClass(BigramComposite.class)
public class Bigram extends Gram implements Serializable {
    @Id
    private String firstword;
    @Id
    private String secondword;
    private int count;

    public String getFirstword() {
        return firstword;
    }

    public void setFirstword(String firstword) {
        this.firstword = firstword;
    }

    public String getSecondword() {
        return secondword;
    }

    public void setSecondword(String secondword) {
        this.secondword = secondword;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}

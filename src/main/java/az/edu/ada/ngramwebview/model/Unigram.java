package az.edu.ada.ngramwebview.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Unigram extends Gram{

    @Id
    private String firstword;
    private int count;

    public String getFirstword() {
        return firstword;
    }

    public void setFirstword(String firstword) {
        this.firstword = firstword;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}

package az.edu.ada.ngramwebview.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import java.io.Serializable;

@Entity
@IdClass(FivegramComposite.class)
public class Fivegram extends Gram implements Serializable {
    @Id
    private String firstword;
    @Id
    private String secondword;
    @Id
    private String thirdword;
    @Id
    private String fourthword;
    @Id
    private String fifthword;
    private int count;

    public String getFirstword() {
        return firstword;
    }

    public void setFirstword(String firstword) {
        this.firstword = firstword;
    }

    public String getSecondword() {
        return secondword;
    }

    public void setSecondword(String secondword) {
        this.secondword = secondword;
    }

    public String getThirdword() {
        return thirdword;
    }

    public void setThirdword(String thirdword) {
        this.thirdword = thirdword;
    }

    public String getFourthword() {
        return fourthword;
    }

    public void setFourthword(String fourthword) {
        this.fourthword = fourthword;
    }

    public String getFifthword() {
        return fifthword;
    }

    public void setFifthword(String fifthword) {
        this.fifthword = fifthword;
    }

    public int getCount() {
        return count;
    }


    public void setCount(int count) {
        this.count = count;
    }


}
